﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Common;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Homework_University1
{
    public partial class Department : Form
    {
        List<string> deps = new List<string>();
        public Department()
        {
            //LoadFromDB();
            InitializeComponent();
        }

        private void LoadFromDB()
        {
            listViewMain.Items.Clear();
            IDbConnection connection = new MySqlConnection(
                            new MySqlConnectionStringBuilder
                            {
                                Server = "127.0.0.1",
                                Database = "TestDatabase",
                                UserID = "sa",
                                Password = "root"
                            }.ConnectionString);
            try
            {
                connection.Open();

                IDbCommand command = connection.CreateCommand();
                command.CommandText = "USE university";
                command.ExecuteNonQuery();
                command.CommandText = "SELECT * FROM tbdepartments";
                command.ExecuteNonQuery();
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    deps.Add(reader.GetString(1));
                    listViewMain.Items.Add($"[{reader.GetInt32(0)}] {reader.GetString(1)}");
                }
                connection.Close();
                listViewMain.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LoadToDB(string dep)
        {
            IDbConnection connection = new MySqlConnection(
                            new MySqlConnectionStringBuilder
                            {
                                Server = "127.0.0.1",
                                Database = "TestDatabase",
                                UserID = "sa",
                                Password = "root"
                            }.ConnectionString);
            try
            {
                connection.Open();

                IDbCommand command = connection.CreateCommand();
                command.CommandText = "USE university";
                command.ExecuteNonQuery();
                command.CommandText = string.Format($"INSERT INTO tbdepartments (Name) VALUES ('{dep}')");
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void DeleteFromDB(int index)
        {
            IDbConnection connection = new MySqlConnection(
                            new MySqlConnectionStringBuilder
                            {
                                Server = "127.0.0.1",
                                Database = "TestDatabase",
                                UserID = "sa",
                                Password = "root"
                            }.ConnectionString);
            try
            {
                connection.Open();

                IDbCommand command = connection.CreateCommand();
                command.CommandText = "USE university";
                command.ExecuteNonQuery();
                command.CommandText = string.Format($"DELETE FROM tbdepartments WHERE Id='{index}'");
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(textBoxName.Text))
            {
                errorProvider.SetError(textBoxName, "Field should't be blank!");
            }
            else if (deps.Contains(textBoxName.Text))
            {
                errorProvider.SetError(textBoxName, "This department is already exist!");
            }
            else
            {
                errorProvider.Clear();
                LoadToDB(textBoxName.Text);
                LoadFromDB();
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            LoadFromDB();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            int index = Int32.Parse(listViewMain.SelectedItems[0].ToString().Split(new string[] { "[", "]" }, StringSplitOptions.RemoveEmptyEntries)[1]);
            DeleteFromDB(index);
            LoadFromDB();
        }
    }
}
